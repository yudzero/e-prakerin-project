<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PemetaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pemetaan')->insert([
            'id_pendaftaran'  =>  "83629",
            'level'  =>  "3543",
            'email'  =>  "35433",
            'password'  =>  bcrypt("1234"),
        ]);
    }
}
