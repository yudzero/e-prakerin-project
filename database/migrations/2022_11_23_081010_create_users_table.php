<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('level');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('nis')->nullable();
            $table->unsignedBigInteger('NoPerusahaan')->nullable();
            $table->unsignedBigInteger('nip')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('nis')->references('nis')->on('siswas');
            $table->foreign('NoPerusahaan')->references('NoPerusahaan')->on('perusahaan');
            $table->foreign('nip')->references('nip')->on('guru');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
