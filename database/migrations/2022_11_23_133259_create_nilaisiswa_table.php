<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilaisiswa', function (Blueprint $table) {
            $table->id();
            $table->string('pkp');
            $table->string('ki');
            $table->string('mm');
            $table->string('kreativitas');
            $table->string('kt');
            $table->string('dt');
            $table->string('pk');
            $table->string('kmm');
            $table->string('kmassk');
            $table->string('kk');
            $table->string('ik');
            $table->string('ppt');
            $table->string('pak');
            $table->unsignedBigInteger('nis');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilaisiswa');
    }
};
