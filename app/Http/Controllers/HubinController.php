<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Pemetaan;
use App\Models\Perusahaan;
use App\Imports\UserImport;
use Illuminate\Http\Request;
use App\Imports\TeacherImport;
use App\Imports\StudentsImport;
use App\Models\Jurusan;
use Barryvdh\DomPDF\Facade\PDF;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class HubinController extends Controller
{

    public function pemetaan(Request $request){
        $pemetaan = Pemetaan::with('siswas')->latest()->get();
        
        return view('hubin.peta', [
            'title' =>  'Pemetaan PKL',
            'titleheader'   =>  'Pemetaan',
            'pemetaan'  =>  $pemetaan
        ]);
    }

    public function tambahpeta(Request $request){
        $data = DB::table('pemetaan')->insert([
            'id_pendaftaran'    => $request->id_pendaftaran,
            'id_periode'    => $request->id_periode,
            'NoPerusahaan'    => $request->NoPerusahaan,
            'nis'    => $request->nis,
            'id_pembimbing'    => $request->id_pembimbing,
        ]);

        return redirect('/hubin/pemeta');
    }

    public function dashboardhubin(){
        $perusahaan = Perusahaan::all();
        $siswa  = Siswa::with('perusahaan')->get();
        return view('hubin.dashboardhubin', [
            'title' =>  'Dashboard | Hubin',
            'titleheader'   =>  'Dashboard',
            'perusahaan'          =>  $perusahaan,
            'siswa'          =>  $siswa,
        ]);
    }
    public function index(){
        return view('perusahaanhubin', [
            'title' => 'Daftar Perusahaan'
        ]);
    }


    public function hubinperusahaan(){
        $jurusan = Jurusan::all();
        $perusahaan = Perusahaan::all();
        return view('hubin.perusahaanhubin', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan',
            'perusahaan'    =>  $perusahaan,
            'jurusan'   =>  $jurusan
        ]);
    }

    public function hubinpemetaan(){
        return view('hubin.pemetaanpkl', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan'
        ]);
    }

    public function tampiljurusan(){
        $jurusan = Jurusan::all();
        return view('hubin.jurusan', [
            'title' =>  'Daftar Jurusan',
            'titleheader'   =>  'Daftar Jurusan',
            'jurusan' => $jurusan
        ]);
    }

    public function edit_jurusan(Request $request, $id_jurusan){
        $jurusan = Jurusan::where('id_jurusan', $request->id);
        $jurusan->update($request->except(['_token']));

        return redirect('hubin/jurusan') ->with('success','Data Berhasil Diubah') ;

    }

    public function edit_perusahaan(Request $request, $NoPerusahaan){
        $perusahaan = Perusahaan::where('NoPerusahaan', $request->id);
        $perusahaan->update($request->except(['_token']));

        return redirect('/  hubin/perusahaan') ->with('success','Data Berhasil Diubah') ;

    }


    public function tambahdatajurusan(Request $request){
        DB::table('jurusan')->insert([
            'id_jurusan' => $request->id_jurusan,
            'jurusan' => $request->jurusan,
        ]);

        

        return redirect('/hubin/jurusan') ->with('success','Data Berhasil Ditambah');
    }

    public function hubineditakunsiswa (){
        return view('hubin.editakunsiswa', [
            'title' =>  'Edit Siswa',
            'titleheader'   =>  'Edit Siswa'
        ]);
    }

    public function siswaterdaftarhubin(){

        
        // $siswa = Pemetaan::where('status', 'diterima')->with('perusahaan', 'jurusan')->get();
        $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])->latest()->get();
        return view('hubin.siswaterdaftarhubin', [
            'title' =>  'Hubin | Siswa Terdaftar',
            'titleheader'   =>  'Daftar Perusahaan',
            'data' => $data
        ]);
    }
    
    public function daftarsiswahubin(){
        $perusahaan = Perusahaan::all();
        $siswa = Siswa::with('perusahaan', 'jurusan')->get();
        $jurusan = Jurusan::all();
        
        return view('hubin.daftarsiswahubin', [
            'title' =>  'Hubin | Daftar Siswa',
            'titleheader'   =>  'Daftar Siswa',
            "siswa" => $siswa,
            "perusahaan"    => $perusahaan,
            "jurusan"   => $jurusan
            
        ]);
    }

    public function cetaksurat(){
        $perusahaan = Perusahaan::all();

        
        $data = Perusahaan::all();

        return view('hubin.cetaksurat', [
            'title' =>  'Hubin | Cetak Surat',
            'titleheader'   =>  'Cetak Surat',
            'perusahaan'    =>  $perusahaan,
            'data'          =>  $data
        ]);
    }

    public function pemetaansiswa(){

        $data = Perusahaan::with(['pemetaan' => function($q){
            return $q->where('status', 'pending')->with('siswa');
        }, 'guru'])->latest()->get();
        
        // $perusahaan = Perusahaan::where('status', ['diterima', 'ditolak'])->with('siswa')->get();
        // $data = Perusahaan::has('siswa')->has('guru')->with('siswa', 'guru')->latest()->get();

        // $pemetaan = DB::table('pemetaan')
        // ->join('perusahaan','perusahaan.NoPerusahaan','=','pemetaan.NoPerusahaan')
        // ->where('perusahaan.NamaPerusahaan')
        // ->get();
        // $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])
        // ->latest()
        // ->orderBy('NoPerusahaan')
        // ->groupBy('NoPerusahaan')
        // ->get();

        $guru  = Guru::all();
        return view('hubin.pemetaansiswa', [
            'title' =>  'Hubin | Pemetaan Siswa',
            'titleheader'   =>  'Pemetaan Siswa',
            'data'  => $data,
            'guru'  => $guru,
            
            // 'perusahaan'    =>  $perusahaan
        ]);
    }


    public function terimaSiswa(Request $request) {

       
        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
            'nip'   => $request->nip,
            'status' => 'diterima',
        ]);
        
        return back()->with('success', 'Siswa diterima');
    }

    public function tolakSiswa(Request $request) {
        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
            'status' => 'ditolak',
        ]);
        
        return back()->with('error', 'Siswa Ditolak!');
    }

    public function pemetaanguru(){
        
        $data = Perusahaan::all();
        $guru = Guru::all();
        return view('hubin.pemetaanguru', [
            'title' =>  'Hubin | Pemetaan Guru',
            'titleheader'   =>  'Pemetaan Guru',
            'data'  =>  $data,
            'guru'    =>  $guru
        ]);
    }

    public function updateguru(Request $request, Perusahaan $perusahaan){
        /* $perusahaan->update([
            'nip' => $request->nip
        ]);
        dd($request->nip); */
        // dd($request->all());
        Perusahaan::where('NoPerusahaan', $perusahaan->NoPerusahaan)->update([
            'nip'   => $request->nip
        ]);

        return  redirect()->back();
    }

    // public function importdata(Request $request){
	// 	Excel::import(new UserImport, $request->file('file'));

    //     return back()->with('success','Excel File Berhasil Diimport!');
    // }
    public function importdata(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_account', $nama_file);

        // import data
        Excel::import(new UserImport, public_path('/file_account/' . $nama_file));


        // alihkan halaman kembali
        return back()->with('success', 'Data Berhasil Di Upload!');
    }

    public function tampilimport(){
           $user = User::all();
        return view('hubin.upload',[
            'user'  => $user,
            'title' =>  'Import Data User',
            'titleheader'   =>  'Import Data User'
        ]);
    }

    public function tampilimportguru(){
           $guru = Guru::all();
        return view('hubin.importguru',[
            'guru'  => $guru,
            'title' =>  'Import Data Guru',
            'titleheader'   =>  'Import Data Guru'
        ]);
    } 

    public function importdataguru(Request $request){
        $file = $request->file('file');

		Excel::import(new TeacherImport, $file);

        return back()->with('success','Excel File Berhasil Diimport!');
    }

    public function cetakmurid($NoPerusahaan){
        $data = Perusahaan::find($NoPerusahaan);

        $pdf = PDF::loadView('hubin.surat', [
            'data'  => $data
        ]);

        return $pdf->setPaper('a4', 'portrait')->download('suratpengajuan.pdf');
    }

    public function suratpdf($NoPerusahaan){
        $data = Perusahaan::find($NoPerusahaan)->with(['pemetaan' => function ($query){
            return $query->where('status', 'pending')->with('siswa.jurusan');
        }]);

        /* $perusahaan = Perusahaan::where('pemetaan', function($query){
            return $query->where('status', 'pending');
        })->with('siswa.jurusan')->get(); */

        return view('hubin.surat', [
            'data'  =>  $data
        ]);
    }


    public function tambahdata(Request $request){
        // $validatedData = $request->validate([
        //     'NoPerusahaan' => 'required',
        //     'NamaPerusahaan' => 'required',
        //     'alamat' => 'required',
        //     'email' => 'required',
        //     'jumlahmurid' => 'required',
        //     'fax' => 'required',
        //     'maps' => 'required',
        // ]);
        
        // Perusahaan::create($validatedData);

        DB::table('perusahaan')->insert([
            'NoPerusahaan' => $request->NoPerusahaan,
            'NamaPerusahaan' => $request->NamaPerusahaan,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'jumlahmurid' => $request->jumlahmurid,
            'fax' => $request->fax,
            'maps' => $request->maps,
            'id_jurusan'    =>  $request->id_jurusan
        ]);

        

        return redirect('/hubin/perusahaan') ->with('success','Data Berhasil Ditambah');
    }

    public function tambahdatasiswa(Request $request){
        DB::table('siswas')->insert([
            'nis'   =>  $request->nis,
            'NamaSiswa'  =>  $request->NamaSiswa,
            'id_jurusan'    =>  $request->id_jurusan,
            'kelas' => $request->kelas,
            'Tgllahir' => $request->Tgllahir,
            'Tmplahir' => $request->Tmplahir,
            'Alamat_Siswa' => $request->Alamat_Siswa,
            'NoTelp' => $request->NoTelp,
            'email'  =>  $request->email
        ]);

        return redirect('/hubin/siswa')->with('success','Berhasil Menambah Data!');
    }

    public function hapusjurusan($id){
        DB::table('jurusan')->where('id_jurusan', $id)->delete();

        return redirect('/hubin/jurusan')->with('info','Data berhasil Dihapus');
    }

    public function hapussiswa($id){
        DB::table('siswas')->where('nis', $id)->delete();

        return redirect('/hubin/siswa')->with('info','Data berhasil Dihapus');
    }

    public function hapusperusahaan($id){
        DB::table('perusahaan')->where('NoPerusahaan', $id)->delete();

        return redirect('/hubin/perusahaan');
    }

    public function updatesiswa(Request $request, $nis){
        $siswa = Siswa::with('pemetaan')->where('nis', $request->id);
        $siswa->update($request->except(['_token']));

        return back()->with('success','Data berhasil Diubah');
    }

    public function tampilimportsiswa(){
        return view('hubin.importsiswa', [
            'title' =>  'Hubin | Import Siswa',
            'titleheader'   =>  'Import Data Siswa'
        ]);
    }

    public function importsiswa(Request $request){

        $file = $request->file('file');

		Excel::import(new StudentsImport, $file);

        return back()->with('success','Excel File Berhasil Diimport!');

    }
    public function pemetaankompetensi(){

        return view('hubin.pemetaankompetensi', [
            'title' =>  'Hubin | Pemetaan Kompetensi',
            'titleheader'   =>  'Pemetaan Kompetensi PKL',
      
        ]);
    }
    
}