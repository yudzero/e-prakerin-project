<?php

namespace App\Http\Controllers;

use App\Models\NilaiSiswa;
use App\Models\Siswa;
use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function dashboardsekolah(){
        return view('pembimbing-sekolah.berandasekolah', [
            'title' => 'Dashboard | Pembimbing Sekolah',
            'titleheader' => 'Dashboard'
        ]);
    }
    public function evaluasipkl(){
        return view('pembimbing-sekolah.evaluasipkl', [
            'title' =>  'Dashboard | Evaluasi PKL',
            'titleheader'   =>  'Evaluasi PKL'
        ]);
    }

    public function viewsikapsiswa($nis){
        // $siswa = Siswa::find($nis);
        $data = NilaiSiswa::with('siswa')->where('nis', $nis)->first();
        $siswa = Siswa::find($nis)->with('jurusan')->first();
        return view('pembimbing-sekolah.viewsikapsiswa', [
            'title' => 'Dashboard | Nilai Sikap Siswa',
            'titleheader' => 'Nilai Sikap Siswa',
            'data'  =>  $data,
            'siswa' =>   $siswa
            
        ]); 
    }

    public function daftrasiswasekolah(){
        $data = Siswa::with('jurusan', 'perusahaan')->get();
        return view('pembimbing-sekolah.daftarsiswasekolah',[
            'title' => 'Dashboard | Daftar Siswa',
            'titleheader' => 'Daftar Siswa',
            'data'  =>  $data
        ]);
    }
    
    
}
