<?php

namespace App\Http\Controllers;

use App\Models\NilaiSiswa;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\Siswa;

class PerusahaanController extends Controller
{
    public function pembimbingperusahaan(){
        $siswa = Siswa::all();
        return view('pembimbing-perusahaan.pembimbing-perusahaan', [
            'title' =>  'Dashboard | Pembimbing Perusahaan',
            'titleheader'   =>  'Pembimbing Perusahaan',
            'siswa' => $siswa
        ]);
    }

    public function detaildata(){
        return view('pembimbing-perusahaan.detail-data', [
            'title' => 'Detail Data',
            'titleheader' => 'Detail Data'
        ]);
    }

    public function daftarsiswa(){
        $perusahaan = Perusahaan::where('nip', auth()->user()->username)->with('siswa.jurusan')->first();

        // dd($perusahaan);
        // $siswa = Siswa::all();
        return view('pembimbing-perusahaan.daftarsiswa', [
            'title' => 'Daftar Siswa',
            'titleheader' => 'Daftar Siswa',
            'siswa' => $perusahaan
        ]);
    }

    public function berinilai($nis){
        $siswa = Siswa::find($nis);
        return view('pembimbing-perusahaan.berinilai', [
            'title' => 'Beri Nilai',
            'titleheader' => 'Beri Nilai',
            'siswa' =>  $siswa
        ]);
    }

    public function postnilai(Request $request){
        
        // $data = $this->validate($request, [
        //     'pkp'   =>  'required|numeric',
        //     'ki'   =>  'required|numeric',
        //     'mm'   =>  'required|numeric',
        //     'kreativitas'   =>  'required|numeric',
        //     'kt'   =>  'required|numeric',
        //     'dt'   =>  'required|numeric',
        //     'pk'   =>  'required|numeric',
        //     'kmassk'   =>  'required|numeric',
        //     'kk'   =>  'required|numeric',
        //     'ik'   =>  'required|numeric',
        //     'ppt'   =>  'required|numeric',
        //     'pak'   =>  'required|numeric'
        // ]);
        
        NilaiSiswa::create([
            'pkp'   =>  $request->pkp,
            'ki'   =>  $request->ki,
            'mm'   =>  $request->mm,
            'kreativitas'   =>  $request->kreativitas,
            'kt'   =>  $request->kt,
            'dt'   =>  $request->dt,
            'pk'   =>  $request->pk,
            'kmm'   =>  $request->kmm,
            'kmassk'   =>  $request->kmassk,
            'kk'   =>  $request->kk,
            'ik'   =>  $request->ik,
            'ppt'   =>  $request->ppt,
            'pak'   =>  $request->pak,
            'nis'   => $request->nis
        ]);

        return redirect('/dashboard/pembimbingperusahaan')->with('success', 'Berhasil Memberi Nilai!');
    }
}
