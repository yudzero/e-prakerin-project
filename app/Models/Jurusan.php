<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    use HasFactory;

    protected $table = 'jurusan';
    protected $fillable = ['id_jurusan', 'jurusan'];
    protected $primaryKey = 'id_jurusan';

    public function siswa(){
        return $this->hasMany(Siswa::class);
    }

    public function pemetaan(){
        return $this->hasMany(Pemetaan::class);
    }

    public function perusahaan(){
        return $this->hasMany(Perusahaan::class, 'NoPerusahaan');
    }
}