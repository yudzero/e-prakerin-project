<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

// class UserImport implements ToModel, WithHeadingRow
// {
//     /**
//     * @param array $row
//     *
//     * @return \Illuminate\Database\Eloquent\Model|null
//     */
//     public function model(array $row)
//     {
//         $row['nis'] = Siswa::where("NamaSiswa", "like", "%".$row['nis']."%");
//         $row['line_manager_id']         = HrEmployee::where("first_name", "like", "%".$row['line_manager']."%");
//         $row['employee_job_title_id']   = HrJobTitle::where("name", "like", "%".$row['job_title']."%");

        
//         // dd($row);
//         /* var_dump($row);
//         die(); */
//         return new User([
//             'username'  =>  $row['username'],
//             'level'  =>  $row['level'],
//             'email'  =>  $row['email'],
//             'password'  =>  Hash::make($row['password']),
//             'nis' => $row['nis'],
//             'NoPerusahaan' => $row['noperusahaan'],
//             'nip'  => $row['nip']
//         ]);
//     }
// }

class UserImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new User([
            'username' => $row[0],
            'level' => $row[1],
            'email' => $row[2],
            'password' => Hash::make($row[3]),
            'nis' => $row[4],
            'NoPerusahaan' => $row[5],
            'nip' => $row[6],
        ]);
    }
}
