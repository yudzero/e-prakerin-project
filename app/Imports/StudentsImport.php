<?php

namespace App\Imports;

use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        dd($row);
        return new Siswa([
            'nis' => $row['nis'],
            'id_jurusan' => $row['id_jurusan'],
            'NamaSiswa' => $row['NamaSiswa'],
            'kelas' => $row['kelas'],
            'Tgllahir' => $row['tgllahir'],
            'Tmplahir' => $row['tmplahir'],
            'Alamat_Siswa' => $row['alamat_siswa'],
            'NoTelp' => $row['notelp'],
            'email' => $row['email'],
        ]);
    }
}
