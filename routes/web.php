<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HubinController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\PerusahaanController;
use App\Models\Perusahaan;
use Database\Seeders\SiswaSeeder;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route Hubin */
Route::get('/', function () {
    return view('welcome', [
        "title" =>  "Dashboard | Hubin",
        "titleheader" =>  "Dashboard"
    ]);
});




/* Route Siswa */



/* Route Pembimbing Perusahaan */




/* Route Pembimbing Sekolah */



// Route::get('/hubin/perusahaan', [HubinController::class, 'index']);

Route::group(['middleware' => 'prevent-back-history'],function(){
/*Route Login*/
Route::get('/login', [LoginController::class, 'viewlogin'])->name('viewlogin');

Route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth','ceklevel:hubin'])->group(function(){
    route::get('/dashboard/hubin', [HubinController::class, 'dashboardhubin'])->name('dashboardhubin');
    Route::get('/hubin/perusahaan', [HubinController::class, 'hubinperusahaan'])->name('hubinperusahaan');
    Route::get('/hubin/tambahperusahaan', [HubinController::class, 'tambahperusahaan'])->name('tambahperusahaan');
    Route::post('/tambah/perusahaan', [HubinController::class, 'tambahdata'])->name('tambahdata');
    Route::post('/tambah/jurusan', [HubinController::class, 'tambahdatajurusan'])->name('tambahdatajurusan');
    Route::post('/tambah/siswa', [HubinController::class, 'tambahdatasiswa'])->name('tambahdatasiswa');
    Route::get('/hubin/jurusan', [HubinController::class, 'tampiljurusan'])->name('tampiljurusan');
    Route::post('/hubin/{id}/edit_perusahaan', [HubinController::class, 'edit_perusahaan']);
    Route::post('/hubin/{id}/edit_jurusan', [HubinController::class, 'edit_jurusan']);
    Route::post('/hubin/{id}/edit_siswa', [HubinController::class, 'updatesiswa']);
    Route::get('/hapus/perusahaan/{id}', [HubinController::class, 'hapusperusahaan'])->name('hapusperusahaan');
    Route::get('/hapus/jurusan/{id}', [HubinController::class, 'hapusjurusan'])->name('hapusjurusan');
    Route::get('/hapus/siswa/{id}', [HubinController::class, 'hapussiswa'])->name('hapussiswa');
    Route::get('/hubin/editakunsiswa', [HubinController::class, 'hubineditakunsiswa'])->name('hubineditakunsiswa');
    Route::get('/hubin/pemetaan', [HubinController::class, 'hubinpemetaan'])->name('hubinpemetaan');
    Route::get('/hubin/pemetaanguru', [HubinController::class, 'pemetaanguru'])->name('pemetaanguru');
    Route::get('/hubin/siswaterdaftar', [HubinController::class, 'siswaterdaftarhubin'])->name('siswaterdaftarhubin');
    Route::get('/hubin/siswa', [HubinController::class, 'daftarsiswahubin'])->name('daftarsiswahubin');
    Route::get('/hubin/cetaksurat', [HubinController::class, 'cetaksurat'])->name('cetaksurat');
    Route::get('/cetakmurid/{NoPerusahaan}', [HubinController::class, 'cetakmurid'])->name('data.pdf');
    Route::post('/hubin/importdata', [HubinController::class, 'importdata'])->name('importdata');
    Route::get('/hubin/importdata', [HubinController::class, 'tampilimport'])->name('tampilimport');
    Route::get('/hubin/pemeta', [HubinController::class, 'pemetaan'])->name('pemetaan');
    Route::get('/hubin/pemetaansiswa', [HubinController::class, 'pemetaansiswa'])->name('pemetaansiswa');
    Route::get('/hubin/pemetaankompetensi', [HubinController::class, 'pemetaankompetensi'])->name('pemetaankompetensi');
    Route::post('/tambahpeta', [HubinController::class, 'tambahpeta'])->name('tambahpeta');
    Route::get('/cetaksurat', [HubinController::class, 'suratpdf'])->name('suratpdf');
    Route::get('/hubin/importsiswa', [HubinController::class, 'tampilimportsiswa']);
    Route::get('/hubin/importguru', [HubinController::class, 'tampilimportguru']);
    Route::post('/importsiswa', [HubinController::class, 'importsiswa']);
    Route::post('/importguru', [HubinController::class, 'importdataguru'])->name('importGuru');
    Route::put('/terima-siswa/{nis}', [HubinController::class, 'terimaSiswa'])->name('terimaSiswa');
    Route::put('/tolak-siswa/{nis}', [HubinController::class, 'tolakSiswa'])->name('tolakSiswa');
    Route::put('/updateguru/{perusahaan:NoPerusahaan}', [HubinController::class, 'updateGuru']);
    Route::post('/nopal', function(Request $request){
        dd($request);
    });
});

Route::middleware(['auth','ceklevel:siswa'])->group(function(){
    Route::get('/dashboard/siswa', [SiswaController::class, 'berandasiswa'])->name('berandasiswa');
    Route::get('/siswa/jurnal', [SiswaController::class, 'jurnalsiswa'])->name('jurnalsiswa');
    Route::get('/siswa/isijurnal', [SiswaController::class, 'isijurnal'])->name('isijurnal');
    Route::post('/isijurnal', [SiswaSeeder::class, 'siswajurnal'])->name('siswajurnal');
    Route::get('/siswa/sikap', [SiswaController::class, 'sikapsiswa'])->name('sikapsiswa');
    Route::get('/siswa/daftarpkl', [SiswaController::class, 'daftarindustrisiswa'])->name('daftarindustrisiswa');
    Route::get('/siswa/profil', [SiswaController::class, 'profilsiswa'])->name('profilsiswa');
    Route::put('/updateprofil/{siswa:nis}', [SiswaController::class, 'updateprofil'])->name('update.profil');
    // Route::get('/siswa/beranda', [SiswaController::class, 'berandasiswa'])->name('berandasiswa');
    Route::get('/siswa/absenhadir', [SiswaController::class, 'absenhadir'])->name('absen-hadir');
    Route::get('/siswa/absentidakhadir', [SiswaController::class, 'absentidakhadir'])->name('absentidakhadir');
    Route::get('/siswa/formdaftar', [SiswaController::class, 'formdaftar'])->name('formdaftar');
    Route::post('/siswadaftar', [SiswaController::class, 'masukpemetaan']);

});

Route::middleware(['auth','ceklevel:pembimbing perusahaan'])->group(function(){
    Route::get('/dashboard/pembimbingperusahaan', [PerusahaanController::class, 'pembimbingperusahaan'])->name('pembimbingperusahaan');
    Route::post('postnilai', [PerusahaanController::class, 'postnilai'])->name('postNilai');
    Route::get('/perusahaan/detaildata', [PerusahaanController::class, 'detaildata'])->name('detaildata');
    Route::get('/perusahaan/daftarsiswa', [PerusahaanController::class, 'daftarsiswa'])->name('daftarsiswa');
    Route::get('/perusahaan/berinilai/{nis}', [PerusahaanController::class, 'berinilai'])->name('berinilai');
});

Route::middleware(['auth','ceklevel:pembimbing sekolah'])->group(function(){
    Route::get('/dashboard/pembimbingsekolah', [SekolahController::class, 'dashboardsekolah'])->name('dashboardsekolah');
    Route::get('/evaluasipkl', [SekolahController::class, 'evaluasipkl'])->name('evaluasipkl');
    Route::get('/sekolah/siswa', [SekolahController::class, 'daftrasiswasekolah'])->name('daftarsiswasekolah');
    Route::get('/siswa/sikap/{nis}', [SekolahController::class, 'viewsikapsiswa'])->name('viewsikapsiswa');
});

});