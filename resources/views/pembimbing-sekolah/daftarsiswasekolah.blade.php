@extends('layouts.pembimbingsekolah.main')
@section('content')
    <div class="container">
        <div class="row">
        </div>
        <section>
            <div class="content-body">
                <div class="container mb-5">
                    <div class="card">
                        <p class="mt-4 ml-5" style="color:black; font-weight:700;">Semua Murid</p> 
                        <div class="dropdown">
                            <div class="row">
                                <div class="col-8">
                                </div>
                            <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                              Periode
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">Januari-Juni</button>
                              <button class="dropdown-item" type="button">Juni-Desember</button>
                            </div>
                          <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                Tahun
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">2022</button>
                              <button class="dropdown-item" type="button">2023</button>
                              <button class="dropdown-item" type="button">2024</button>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                              Divisi
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">IT</button>
                              <button class="dropdown-item" type="button">Marketing</button>
                            </div>
                        </div>
                          </div>
                        <table class="tabelperusahaan mb-5 mt-3">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Nama Perusahaan</th>
                                <th>Periode(Bulan)</th>
                                <th>Aksi</th>
                            </tr>
                            @foreach ($data as $d)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $d->NamaSiswa }}</td>
                                <td>{{ $d->jurusan->jurusan }}</td>
                                <td>{{ $d->kelas }}</td>
                                <td>{{ $d->perusahaan->NamaPerusahaan }}</td>
                                <td>Januari-Juni</td>
                                <td><a href="/siswa/sikap/{{ $d->nis }}" class="btn btn-circle btn-primary"><i class="fa-solid fa-magnifying-glass"></i></a></td>
                            </tr>
                            @endforeach
                        </table>
                        <hr>
                        <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i class="fa-solid fa-chevron-right"></i> </span></p>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection