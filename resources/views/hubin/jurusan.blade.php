@extends('layouts.hubin.main')
@section('content')

<section>
    <button class="btn btn-primary ml-5" data-toggle="modal" data-target="#tambah-perusahaan"> 
        Tambah Data    
    </button><br><br>

    <div class="modal fade" id="tambah-perusahaan" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/tambah/jurusan" method="POST">
                @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="inputPassword5" class="form-label">id jurusan</label>
                    <input type="text"  name="id_jurusan" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Nama jurusan</label>
                    <input type="text"  name="jurusan" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form>
        </div>
    </div>

    <div class="content-body">
        <div class="container mb-5">
            <div class="card">
                <p class="mt-4 ml-5" style="color:black; font-weight:700;">Jurusan</p>
                <table id="myTable" class="tab mb-5 mt-3">
                    <tr>
                        <th>No.</th>
                        <th>Id Jurusan</th>
                        <th>Nama Jurusan</th>
                        <th>Aksi</th>
                    </tr>
                    @foreach ($jurusan as $p)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $p->id_jurusan }}</td>
                        <td>{{ $p->jurusan }}</td>
                        <td> 
                            <a href="/hapus/jurusan/{{ $p->id_jurusan }}" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                    <!-- Modal -->
                    @endforeach
                </table>
                @foreach($jurusan as $p)
                <div class="modal fade" id="perusahaan-edit-{{ $p->id_jurusan }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        
                        <form action="/hubin/{{ $p->id_jurusan }}/edit_jurusan" method="POST">
                            @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Data {{ $p->id_jurusan }}</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <label for="inputPassword5" class="form-label">Id Jurusan</label>
                                <input type="text" value="{{ $p->id_jurusan }}" name="id_jurusan" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                                <label for="inputPassword5" class="form-label">Nama Jurusan</label>
                                <input type="text" value="{{ $p->jurusan }}" name="jurusan" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                @endforeach
                <hr>
                <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span
                            class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i
                                class="fa-solid fa-chevron-right"></i> </span></p>
                <!-- Button trigger modal -->



            </div>
        </div>
    </div>

 
    
</section>
@endsection