@extends('layouts.hubin.main')
@section('content')
<div class="container">
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    
    @foreach($data as $d)
    <div class="accordion2 ml-4 mt-4">
        <div class="row">
            <div class="col-7">
                <p class="ml-4 mt-4" style="font-weight: 600; font-size:18px;">{{ $d->NamaPerusahaan }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr>
            </div>
        </div>

        

        @foreach($d->pemetaan as $item)
            <div class="row">
                <div class="col-9">
                    <p class=" ml-4">{{ $item->siswa->NamaSiswa }}</p>
                </div>
                    <form action="{{ route('terimaSiswa', $item->siswa->nis) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="nip" value="{{ $d->guru ? $d->guru->nip : '' }}">
                    <div class="col-1 ml-3">
                        <button type="submit" style="text-decoration: none; border:none; background-color:white;"> <i class="fa-solid fa-circle-check" style="color: rgb(39, 220, 39);text-decoration: none; font-size:24px;"></i></button>
                    </div>
                </form>
                <form action="{{ route('tolakSiswa', $item->siswa->nis) }}" method="post">
                    @csrf
                    @method('PUT')
                <div class="col-1">
                    <button style="text-decoration: none; border:none; background-color:white;"><i class="fa-solid fa-ban" style="color: red; font-size:22px;"></i></button>
                </div>
                </form>
            </div>
        @endforeach
        
       </div>
        @endforeach
        
    

    <div class="row mt-4">
        <div class="col-8">

        </div>
        {{-- @foreach($app as $ap)
        {{ $ap->id }}
        @endforeach
        {!! $app->render() !!} --}}
        </div>
        
    </div>
    </div>
</div>
@endsection
