@extends('layouts.hubin.main')
@section('content')
<div class="body">
    
@foreach($data as $d)
    <form action="/updateguru/{{ $d->NoPerusahaan }}" method="post">
        @csrf
        @method('put')
        <div class="box-pemetaan ml-5 mt-4 mb-4">
            <div class="row">
            <div class="col-8 mt-4 ml-4">
            <p style="font-weight: 600; font-size:18px;">{{ $d->NamaPerusahaan }}</p>
            </div>
            <div class="row ">
                <select class="mt-4  pilih-pembimbing" name="nip" id="nip">
                    <option value="" disabled selected>Pilih Pembimbing</option>
                    @foreach($guru as $g)
                        <option @if ($d->nip == $g->nip) selected @endif value="{{ $g->nip }}">{{ $g->nama }}</option>
                    @endforeach
                </select>
                <button type="submit" class="buttonsave ml-4" style="height: 40px; margin-top:20px;">Save</button>
            </div>
            </div>
            </div>  
    </form>
@endforeach
</div>
@endsection