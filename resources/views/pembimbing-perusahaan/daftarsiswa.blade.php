@extends('layouts.pembimbingperusahaan.main')
@section('content')


    <section>
        <div class="content-body">
            <div class="container mb-5">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p>
                    <div class="dropdown">
                        <div class="row">
                            <div class="col-8">
                            </div>
                      <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            Tahun
                        </button>
                        <div class="dropdown-menu">
                          <button class="dropdown-item" type="button">2022</button>
                          <button class="dropdown-item" type="button">2023</button>
                          <button class="dropdown-item" type="button">2024</button>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                          Divisi
                        </button>
                        <div class="dropdown-menu">
                          <button class="dropdown-item" type="button">IT</button>
                          <button class="dropdown-item" type="button">Marketing</button>
                        </div>
                    </div>
                      </div>
                      <div class="card-body">
                      <table id="myTable" class="table">
                        <thead>
                          <tr style="background-color: #DADADC; border-radius:30px;">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Nama Perusahaan</th>
                                <th>Periode</th>
                                <th>Beri Nilai</th>
                            </tr>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa->siswa as $s)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $s->NamaSiswa }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td>{{ $s->perusahaan->NamaPerusahaan ?? 'Belum Terdaftar' }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td><a href="/perusahaan/berinilai/{{ $s->nis }}" class="btn btn-primary btn-circle"><i class="fa-solid fa-pencil"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </section>

@endsection