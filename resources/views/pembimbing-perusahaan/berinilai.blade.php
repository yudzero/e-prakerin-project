@extends('layouts.pembimbingperusahaan.main')
@section('content')
    <div class="container">
        <h4 class="mt-5">Detail Data Siswa</h4>
        <div class="card mt-4" style="background-color: #EDEDED; border-radius: 20px; border: #EDEDED;">
            <div class="row mt-5 mb-5" style="margin: 0 auto; width: 100%;">
                <div class="col-1">
                </div>
                <div class="col" >
                    <p>Nama</p>
                    <p>Jurusan</p>
                    <p>Kelas</p>
                    <p>Tempat Tanggal Lahir</p>
                    <p>Alamat</p>
                </div>
                <div class="col-1">
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                </div>
                <div class="col">

                    <p>{{ $siswa->NamaSiswa }}</p>
                    <p>{{ $siswa->jurusan->jurusan }}</p>
                    <p>{{ $siswa->kelas }}</p>
                    <p>{{ $siswa->Tmplahir }}, {{ $siswa->Tgllahir }}</p>
                    <p>{{ $siswa->Alamat_Siswa }}</p>

                </div>
            </div>
        </div>


        <div class="row mt-4 mb-3">
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Sikap
            </div>
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Pengetahuan
            </div>
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Keterampilan
            </div>
        </div>
        <form action="{{ route('postNilai') }}" method="post">
            @csrf
        <div class="row mt-4">
            <div class="col-4">
                <label>Penampilan Kerapihan Pakaian</label>
                <input type="number" class="form-control" name="pkp" required>
            </div>
             <div class="col-4">
                <label>Penguasaan Keilmuan</label>
                <input type="number" class="form-control" name="pk" required>
            </div>
             <div class="col-4">
                <label>Keahlian dan Keterampilan</label>
                <input type="number" class="form-control" name="kk" required>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Komitmen dan Integritas</label>
                <input type="number" class="form-control" name="ki" required>
            </div>
             <div class="col-4">
                <label>Kemampuan Mengidentifikasi Masalah</label>
                <input type="number" class="form-control" name="kmm" required>
            </div>
             <div class="col-4">
                <label>Inovasi dan Kreativitas</label>
                <input type="number" class="form-control" name="ik" required>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Menghargai dan Menghormati (Kesopanan)</label>
                <input type="number" class="form-control" name="mm" required>
            </div>
             <div class="col-4">
                <label style="font-size: 12px;">Kemampuan Menemukan Alternatif Solusi Secara Kreatif</label>
                <input type="number" class="form-control" name="kmassk" required>
            </div>
             <div class="col-4">
                <label>Produktivitas dan Penyelesaian Tugas</label>
                <input type="number" class="form-control" name="ppt" required>
            </div>
            
        </div>

        <div class="row ">
            <div class="col-4">
                <label>Kreativitas</label>
                <input type="number" class="form-control" name="kreativitas" required>
            </div>
            <div class="col-4">
                
            </div>
            <div class="col-4">
                <label>Penguasaan Alat Kerja</label>
                <input type="number" class="form-control" name="pak" required>
            </div>  
       
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Kerjasama Tim</label>
                <input type="number" class="form-control" name="kt" required>
            </div>
            <div class="col-4">

            </div> 
        </div>
       

        <div class="row mt-2">
            <div class="col-4">
                <label>Disiplin dan Tanggung jawab</label>
                <input type="number" class="form-control" name="dt" required>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <input type="text" class="form-control" value="{{ $siswa->nis }}" name="nis" hidden>
            </div>
        </div>

        

        <div class="row mt-2 mb-4">
            <div class="col-4">
                <button class="btn btn-success" type="submit">Save</button>
            </div>
        </div>
    </form>
    </div>

@endsection